<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class HeroeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('heroes')->insert([
            'nombre' => 'Ironman',
            'empresa' => 'Marvel',
            'foto' => 'img/ironman.png',
            'descripcion' => 'Anthony Edward Stark conocido como Tony Stark, es un multimillonario magnate empresarial estadounidense, playboy e ingenioso científico, quien sufrió una grave lesión en el pecho durante un secuestro en el Medio Oriente. ... Él usa el traje y las versiones sucesivas para proteger al mundo como Iron Man.',
            'link' => 'https://es.wikipedia.org/wiki/Iron_Man',
            'like' => 60,
            'dislike' => 40,

        ]);
        DB::table('heroes')->insert([
            'nombre' => 'Superman',
            'empresa' => 'DC',
            'foto' => 'img/superman_2.png',
            'descripcion' => 'Superman nació en el planeta Krypton y recibió el nombre de Kal-El al nacer. Cuando era bebé, sus padres, el científico Jor-El, y su esposa Lara Lor-Van.',
            'link' => 'https://es.wikipedia.org/wiki/Superman',
            'like' => 64,
            'dislike' => 36,

        ]);
        DB::table('heroes')->insert([
            'nombre' => 'Thor',
            'empresa' => 'Marvel',
            'foto' => 'img/thor_2.png',
            'descripcion' => 'Thor Odinson es un asgardiano, el es llamado el dios nordico del trueno y un poderoso guerrero en Asgard.',
            'link' => 'https://es.wikipedia.org/wiki/Thor',
            'like' => 36,
            'dislike' => 64,

        ]);
        DB::table('heroes')->insert([
            'nombre' => 'Batman',
            'empresa' => 'DC',
            'foto' => 'img/batman_2.png',
            'descripcion' => 'Batman es un hombre alto, caucásico, tiene una figura oscura e imponente, posee con cabello negro corto y ojos marrones.',
            'link' => 'https://es.wikipedia.org/wiki/Batman',
            'like' => 36,
            'dislike' => 64,

        ]);
        DB::table('heroes')->insert([
            'nombre' => 'Spiderman',
            'empresa' => 'Marvel',
            'foto' => 'img/spiderman_2.png',
            'descripcion' => 'Científico, vigilante, profesor, fotógrafo, superhéroe. Fuerza, velocidad, durabilidad, agilidad, sentidos, reflejos, coordinación, equilibrio y resistencia sobrehumanos.',
            'link' => 'https://es.wikipedia.org/wiki/Spider-Man',
            'like' => 64,
            'dislike' => 36,

        ]);
    }
}
