@extends('layouts.app')

@section('title', 'Test Heroes Votación Laravel Vue')


@section('content')
@include('header')
<main role="main">
    <section class="jumbotron img_p img-fluid">
      <div class="container">
        <div class="row justify-content-start">
          <div class="col-md-6 col-xs-12 my-5">
            <!-- box de votar -->
            <div id="viewVote">
              <div class="row box_p">
                <div class="col-12 p-4">
                  <p class="lead">Dinos tu opinión sobre</p>
                  <h1 id="nameHero">@{{heroe.nombre}}?</h1>
                  <p class="lead" id="descHero">@{{heroe.descripcion}}</p>
                  <p class="lead"><i class="fab fa-wikipedia-w"></i><a :href="heroe.link" target="_blank" id="linkHero"> Más Información</a></p>
                  <h4>¿Cuál es tu voto?</h1>
                </div>
              </div>
              <div class="row">
                <div class="col-6 colVotoNew">
                  <button class="btn btn-success btn-block btn_ok btnVotoNew" id="btnOk" value="like"  @click="addVote(heroe.id,1)"><i class="fas fa-thumbs-up fa-2x"></i></button>
                </div>
                <div class="col-6 colVotoNew">
                  <button class="btn btn-warning btn-block btn_out btnVotoNew" id="btnOut" value="dislike"  @click="addVote(heroe.id,0)"><i class="fas fa-thumbs-up fa-flip-both fa-2x text-white"></i></button>
                </div>
              </div>
            </div>

            <!-- dislike -->
            <div class="row box_p pb-4" id="viewDislike">
              <div class="col-12 p-4">
                <h2>@{{heroe.empresa}} - <strong>@{{heroe.nombre}}</strong></h2>
                <div class="row">
                  <div class="col-auto">
                    <h1><span class="badge badge-warning text-white"><i class="fas fa-thumbs-up fa-flip-both fa-2x"></i></span></h1>
                  </div>
                  <div class="col-md-9">
                    <h2>Tu voto <br> ha sido registrado!</h2>
                  </div>
                </div>
                <p class="lead">@{{heroe.descripcion}}</p>
              </div>
              <div class="col-12">
                <button class="btn btn-outline-light btn-block btn_ok" id="btnBackDislike" value="backDislike" @click="showVote(3)"><h4><i class="fas fa-chevron-left"></i><i class="fas fa-chevron-left"></i> Volver a votar</h4></button>
              </div>
            </div>
            <!-- ok -->
            <div class="row box_p pb-4" id="viewLike">
              <div class="col-12 p-4">
                <h2>@{{heroe.empresa}} - <strong>@{{heroe.nombre}}</strong></h2>
                <div class="row">
                  <div class="col-auto">
                    <h1><span class="badge badge-success"><i class="fas fa-thumbs-up fa-2x"></i></span></h1>
                  </div>
                  <div class="col-md-9">
                    <h2>Tu voto <br> ha sido registrado!</h2>
                  </div>
                </div>
                <p class="lead">@{{heroe.descripcion}}</p>
              </div>
              <div class="col-12">
                <button class="btn btn-outline-light btn-block btn_ok" id="btnBackLike" value="backLike" @click="showVote(2)"><h4><i class="fas fa-chevron-left"></i><i class="fas fa-chevron-left"></i> Volver a votar</h4></button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <div class="progress" style="height: 100px;">
        <div id="progressOk" class="progress-bar bg-success progress-bar-striped progress-bar-animated" role="progressbar" style="width: 88%" aria-valuenow="88" aria-valuemin="0" aria-valuemax="100"><h2><i class="fas fa-thumbs-up"></i><span id="valuePercentageOk">88%</span></h2></div>
        <div id="progressOut" class="progress-bar bg-warning progress-bar-striped progress-bar-animated" role="progressbar" style="width: 12%" aria-valuenow="12" aria-valuemin="0" aria-valuemax="100"><h2><span id="valuePercentageOut">12%</span><i class="fas fa-thumbs-up fa-flip-both"></i></h2></div>
    </div>
    <div class="album py-5 bg-light">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="alert alert-dark alert-dismissible fade show" role="alert">
              <div class="row">
                <div class="col-md-4 col-xs-12">
                  <h2>Lorem ipsum dolor</h2>
                  <h1>Ip Lorem ip</h1>
                </div>
                <div class="col-md-8 col-xs-12">
                  <h5>Aww yeah, you successfully read this important alert message. This example text is going to run a bit longer so that you can see how spacing within an alert works with this kind of content.</h5>
                </div>
              </div>
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
          </div>
        </div>
      </div>
      <div class="container" id="anteriores">
        <div class="row mb-2">
          <div class="col-md-12">
            <h2>Super Heroes anteriores</h2>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6 mb-3" v-for='dato in heroes'>
            <div class="card text-white">
            <img :src="dato.foto" class="card-img" alt="...">
              <div class="card-img-overlay gradiente_img">
                <h1 class="card-title"><span class="badge badge-success"><i class="fas fa-thumbs-up"></i></span> @{{ dato.nombre }}</h1>
                <p class="card-text">@{{ dato.descripcion }}</p>
                <div class="row">
                  <div class="col-6">
                    <button type="button" class="btn btn-block btn-outline-light" @click="showModal(dato)">Ver detalle</button>
                  </div>
                  <div class="col-6 text-right">
                    <p class="card-text">1 mes atrás <br> lorens</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="progress progressNow">
                  <div class="progress-bar bg-success progress-bar-striped progress-bar-animated" role="progressbar" :style="'width:'+ dato.like +'%'" :aria-valuenow="dato.like" aria-valuemin="0" aria-valuemax="100"><h6><i class="fas fa-thumbs-up"></i> @{{ dato.like }}%</h6></div>
                  <div class="progress-bar bg-warning progress-bar-striped progress-bar-animated" role="progressbar" :style="'width:'+ dato.dislike +'%'" :aria-valuenow="dato.dislike" aria-valuemin="0" aria-valuemax="100"><h6>@{{ dato.dislike }}% <i class="fas fa-thumbs-up fa-flip-both"></i></h6></div>
                </div>
              </div>
            </div>
          </div>
          {{-- fin de v-for --}}
        </div>
      </div>
    </div>
    <!-- Modal Superman-->
    <div class="modal fade" id="supermanModal" tabindex="-1" aria-labelledby="supermanModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header bg-success">
            <h5 class="modal-title" id="supermanModalLabel">Superman</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="card">
              <img src="img/superman.png" class="card-img-top" alt="...">
              <div class="card-body">
                <h5 class="card-title">Superman</h5>
                <p class="card-text">Superman nació en el planeta Krypton y recibió el nombre de Kal-El al nacer. Cuando era bebé, sus padres, el científico Jor-El, y su esposa Lara Lor-Van, lo enviaron a la Tierra en una pequeña nave espacial momentos antes de que Krypton fuera destruido en un cataclismo natural.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Modal Thor-->
    <div class="modal fade" id="thorModal" tabindex="-1" aria-labelledby="thorModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header bg-success">
            <h5 class="modal-title" id="thorModalLabel">Thor</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="card">
              <img src="img/thor.png" class="card-img-top" alt="...">
              <div class="card-body">
                <h5 class="card-title">Thor</h5>
                <p class="card-text">Thor Odinson es un asgardiano, el es llamado el dios nordico del trueno y un poderoso guerrero en Asgard.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Modal Batman-->
    <div class="modal fade" id="batmanModal" tabindex="-1" aria-labelledby="batmanModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header bg-success">
            <h5 class="modal-title" id="batmanModalLabel">Batman</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="card">
              <img src="img/batman.png" class="card-img-top" alt="...">
              <div class="card-body">
                <h5 class="card-title">Batman</h5>
                <p class="card-text">Batman es un hombre alto, caucásico, tiene una figura oscura e imponente, posee con cabello negro corto y ojos marrones.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Modal Spiderman-->
    <div class="modal fade" id="spidermanModal" tabindex="-1" aria-labelledby="spidermanModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header bg-success">
            <h5 class="modal-title" id="spidermanModalLabel">Spiderman</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="card">
              <img src="img/spiderman.png" class="card-img-top" alt="...">
              <div class="card-body">
                <h5 class="card-title">Spiderman</h5>
                <p class="card-text">Científico, vigilante, profesor, fotógrafo, superhéroe. Fuerza, velocidad, durabilidad, agilidad, sentidos, reflejos, coordinación, equilibrio y resistencia sobrehumanos.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </main>
  @include('footer')
@endsection

