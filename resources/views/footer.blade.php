<footer class="footer mt-auto py-3">
    <div class="container">
      <hr class="hr_foo">
      <div class="row foo_ter">
        <div class="col-md-8 col-xs-12">
          <div class="row">
            <span class="text-muted col-md-auto col-xs-12">Terminos y condiciones</span>
            <span class="text-muted col-md-auto col-xs-12">Politica de tratamiento de datos</span>
            <span class="text-muted col-md-auto col-xs-12">Contactenos</span>
          </div>
        </div>
        <div class="col-md-4 col-xs-12 network">
          <div class="row">
            <div class="col-12">
              <span class="text-muted mr-3">Siguenos:</span>
              <span class="text-muted mr-3"><i class="fab fa-facebook-square"></i></span>
              <span class="text-muted mr-3"><i class="fab fa-twitter"></i></span>
            </div>
          </div>
        </div>
      </div>
    </div>
  </footer>

