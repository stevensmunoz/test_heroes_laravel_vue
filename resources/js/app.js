/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue').default;
window.Swal = require('sweetalert2');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    data: {
        heroe: [],
        heroes: [],
    },
    methods:{
        getDatos(){
            let url = 'heroe';
            axios.get(url)
            .then( response => {
                let like = response.data.heroe.like;
                let dislike = response.data.heroe.dislike;

                this.heroe = response.data.heroe;
                this.heroes = response.data.heroes;

                this.calculaPercentage(like,dislike);
            });
        },
        showModal(dato){
            Swal.fire({
                title: dato.empresa +' - '+ dato.nombre,
                text: dato.descripcion,
                imageUrl: dato.foto,
                imageWidth: 350,
                imageHeight: 400,
                imageAlt: 'Custom image',
              })
        },
        calculaPercentage(like, dislike){
            let pOk = ((parseInt(like) / (parseInt(like) + parseInt(dislike))) * 100).toFixed(1);
            let pOut = (100 - pOk).toFixed(1);


            // text
            document.getElementById('valuePercentageOk').innerHTML = ' '+pOk+'%';
            document.getElementById('valuePercentageOut').innerHTML = pOut+'% ';
            // aria-valueno
            document.getElementById('progressOk').setAttribute('aria-valuenow', pOk);
            document.getElementById('progressOut').setAttribute('aria-valuenow', pOut);
            // width
            document.getElementById('progressOk').style.width = pOk+'%';
            document.getElementById('progressOut').style.width = pOut+'%';
        },
        addVote(id,voto){

            let url = 'add_vote/'+id+'/'+voto;
            axios.get(url)
            .then( response => {
                let like = response.data.like;
                let dislike = response.data.dislike;
                this.showVote(voto);
                this.calculaPercentage(like,dislike);
            });

        },
        showVote(vote){

            let viewVote = document.getElementById('viewVote');
            let viewLike = document.getElementById('viewLike');
            let viewDislike = document.getElementById('viewDislike');

            switch (vote) {
                case 1:
                    viewVote.style.display = "none";
                    viewLike.style.display = "block";

                    break;
                case 0:
                    viewVote.style.display = "none";
                    viewDislike.style.display = "block";

                    break;
                case 2:
                    viewVote.style.display = "block";
                    viewLike.style.display = "none";

                    break;
                case 3:
                    viewVote.style.display = "block";
                    viewDislike.style.display = "none";

                    break;

                default:
                    break;
            }
        }
    },
    mounted(){
        this.getDatos();
    }

});
